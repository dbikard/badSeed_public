This repository contains the code used in the paper:

**A CRISPRi screen in E. coli reveals an unexpected sequence-specific toxicity of dCas9**
Lun Cui, Antoine Vigouroux, Francois Rousset, Hugo Varet, Varun Khanna & David Bikard


In this study we designed a library of  guide RNAs targeting random positions
along the genome of E. coli MG1655, with the simple requirement of a “NGG” PAM. The library 
contains an average of 19 targets per gene. A pool of guide RNAs obtained through on-chip 
oligo synthesis was cloned under the control of a constitutive promoter on plasmid psgRNA 
and electroporated in strain LC-E18 carrying the dCas9 gene under the control of a Ptet 
promoter in the chromosome. The pooled library of cells was then grown in rich medium 
over 17 generations with anhydrotetracycline (aTc). The fold change in abundance (log2FC) of the guide RNA
was measured through deep sequencing of the library.

The log2FC data for all guides in the screen can be found in the file screen_data.csv

The analysis is divided in three jupyter notebooks:
* Effect of dCas9 binding position and orientation.ipynb
> In this notebook we look at the effect of dCas9 binding position and orientation. In particular we perform an analysis of polar and reverse-polar effects.

* off-target analysis.ipynb
> In this notebook we investigate whether the unexpected fitness defect produced by some guides in the screen can be explained by off-target positions blocking the expression of essential or fitness genes.
  
* bad-seed analysis.ipynb
> In this notebook we use a machine learning approach to reveal how dCas9 can kill _E. coli_ when guided by some specific 5nt PAM-proximal sequences 
  

