import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from Bio import SeqIO
from adjustText import adjust_text

ref = next(SeqIO.parse("ecoli-COLI-K12.fsa", "fasta"))
genomeSize = len(ref.seq)
genes = pd.read_csv("genes.txt", sep="\t")


def plot_pos(left, right, data, colname, ax=None):
    plt.rcParams.update({'font.size': 12})
    cmap = plt.cm.get_cmap('Spectral')
    head_length=(right-left)/20 #adapt the arrow head length to the window
    
    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=(4, 4))
    ax.set_xlim(left, right)
    ax.set_ylim(-12, 3)
    texts = []
    genes_region = genes[(genes["left"] > left - 5000) & (genes["right"] < right + 5000)]
    n = genes_region.shape[0]
    for i in range(n):
        l = genes_region.iloc[i]
        c = cmap(float(i) / n)
        if l["ori"] == "+":
            L = l["left"]
            R = l["right"]
        else:
            R = l["left"]
            L = l["right"]
        
        dx=np.sign(R-L)*max(1,(abs(R - L)-head_length))
        ax.arrow(L, 0, dx, 0, head_width=1, head_length=head_length, width=1, fc=c, ec=c, zorder=-1)
        texts.append(ax.text((L + R) / 2, 3, l["name"], color=c, size=20))

    adjust_text(texts, autoalign='y',
                only_move={'points': 'y', 'text': 'y'})  # , arrowprops=dict(arrowstyle="->", color='r', lw=0.5))

    d = data[(data["pos"] > left) & (data["pos"] < right)]
    ax.scatter(d.loc[d["ori"] == "+", 'pos'].values, d.loc[d["ori"] == "+", colname].values, color="red", zorder=1,
               s=20)
    ax.scatter(d.loc[d["ori"] == "-", 'pos'].values, d.loc[d["ori"] == "-", colname].values, color="blue", zorder=1,
               s=20)
    
    ax.set_xlabel("position")
    ax.set_ylabel("log2FC")
    tleft=(left//1000+1)*1000
    tright=(right//1000+1)*1000
    nticks=4
    ax.set_xticks(list(range(tleft,tright,int((tright-tleft)/nticks))))
    return ax


def plot_gene(gene_name, data, colname, win=4000, save=False, ax=None):
    """
    :type gene_name: str
    """
    left = genes[genes["name"] == gene_name]["left"].values[0] - win
    right = genes[genes["name"] == gene_name]["right"].values[0] + win
    return plot_pos(left, right, data, colname, ax=ax)
    # if save:
    #    fig.savefig('../figures/%s-%s-%i.eps' % (gene_name, colname, win), format='eps')
    # plt.show()


def align_bars(seq1, seq2):
    bars = ""
    for i in range(len(seq1)):
        if seq1[i] == seq2[i]:
            bars += "|"
        else:
            bars += " "
    return bars


def off_targets(guide, s, verbose=0, _cache={}):
    if s not in _cache:
        print("loading off-targets with seed of size %i in cache..." % s)
        i = 0
        _cache[s] = {}

        while True:
            p = ref.seq.find("GG", start=i)
            if p == -1:
                break

            sseq = str(ref.seq[p - s - 1:p - 1])
            if sseq in _cache[s]:
                _cache[s][sseq].append([1, p + 1])
            else:
                _cache[s][sseq] = [[1, p + 1]]  # orientation, position in the genome
            i = p + 1

        i = 0
        revseq = ref.seq.reverse_complement()
        while True:
            p = revseq.find("GG", start=i)
            if p == -1:
                break

            sseq = str(revseq[p - s - 1:p - 1])
            if sseq in _cache[s]:
                _cache[s][sseq].append([0, genomeSize - p - 1])
            else:
                _cache[s][sseq] = [[0, genomeSize - p - 1]]

            i = p + 1

    res = _cache[s].get(guide[-s:], [])

    if verbose:
        # adjust spacer for printing
        if len(guide) > 20:
            guide = guide[-20:]
        elif len(guide) < 20:
            guide = " " * (20 - len(guide)) + guide

        for r in res:
            p = r[1]
            print(r)
            if r[0] == 1:
                target = ref.seq[p - 22:p + 1]
            else:
                target = ref.seq.reverse_complement()[len(ref.seq) - p - 22:len(ref.seq) - p + 1]

            bars = align_bars(guide, target)

            print(guide)
            print(bars)
            print(target)
            print("")

    return np.array(res)
